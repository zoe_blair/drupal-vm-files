Quickly Add a VM to an existing Drupal Project

1. Confirm that the repo doesn’t have a Vagrantfile in the route. If it does you can just `vagrant up`
2. Clone this repo into it’s own folder on your computer: https://bitbucket.org/zoe_blair/drupal-vm-files
3. Drag and drop all files into your existing project. 
4. Edit the default.config.yml file vagrant_hostname and vagrant_machine_name to match your project. These names must be unique on your machine. 
5. Update your settings.php or wp-config.php to match this database. (Note, I've added the drupal settings file to this repo so that password is already set)
     database: drupal
     username: drupal
     password: drupal
6. Run `vagrant up` in the root folder for the project and wait what feels like 20 minutes (longer if this is your first DrupalVM. Vagrant downloads a copy of the box the first time and just applies changes to it for each VM thereafter which is a little faster)
7. Commit those files to the repo and PUSH
8. On the Jenkins server (http://192.168.144.28:8080/) add these to the excludes list (via config)
     --exclude default.config.yml \
     --exclude example.drupal.composer.json \
     --exclude example.drupal.make.yml \
     --exclude provisioning/ \
     --exclude Vagrantfile \	

Troubleshooting.
- If you get a weird NFS error on your exports file (sometimes this happens when you're setting up a bunch) edit your /etc/exports file and remove the previous machines from the bottom of the file. Make sure you get all 3 lines, from vagrant begin to vagrant end. 
- If you're getting a lot of terrible errors and you can't figure out there is a change that the vagrant box is corrupt (this happened to ZB!) so you might need to figure out how to delete that and start over. That's annoying though because downloading a fresh box takes a longgg time.